doRf = function(train, test, labels, quater){
	library(randomForest)

	rf <- randomForest(train, labels, xtest=test)
	results <- levels(labels)[rf$test$predicted]

	# Восстанавливаем множ-во test, конкретно нужно восстановить поле target
	test <- train.original[(quater*3+1):length(train.original[,1]),]

	# Проходим по всем результатам сравнивая их с фактическим значением класс
	# Если значения совпали, то инкрементируем "число попаданий"
	counter <- 0
	for (i in 1:length(test[,1])) {
		if (test[i, length(test[1,])] == results[i]) {
			counter <- counter + 1
		}
	}
	counter/length(test[,1])
}