library(randomForest)

setwd('data')
train.original <- read.csv("train.csv")
train.original <- train.original[sample(nrow(train.original)),]

train <- train.original
test <- read.csv("test.csv")

# Получаем targets для каждой строки из training set
labels <- train[,length(train[1,])]

train <- train[,-1]
train <- train[,-length(train[1,])]
test <- test[,-1]

setwd('../out')

rf <- randomForest(train, labels, xtest=test, ntree=1000)
results <- levels(labels)[rf$test$predicted]

write(results, file="rf_result.csv", ncolumns=1) 
