Для определения качества прогноза, будем использовать поле discrepancy в объекте predicted.

Для начала, нужно изменить функцию sum(), добавив флаг "na.rm = TRUE".

	number_repair.col <- c(number_repair.col, sum(data$number_repair, na.rm = TRUE))

Правильность предсказания будем измерять как среднее арифметическое от абсолютных значений discrepancy. 
Так же будем вычислять качество предсказания отдельно для каждой пары модуль/компонент.

	# predicated.quality - содержит характиристики правильности предсказания для каждой пары модуль/компонент
	predicted.quality <- c()

	for (i in 1:length(module.component[,1])) {

	  # Выбираем предсказания для каждой пары модуль/компонент
	  module    <- as.character(module.component[i, "module_category"])
	  component <- as.character(module.component[i, "component_category"])
	  predicted.module.component <- subset(predicted$discrepancy, 
	    predicted$component_category==component & predicted$module_category==module)

	  # Вычисляем среднее арифиметическое отклонение для каждой пары
	  discrepancy <- 0
	  for (j in 1:length(predicted.module.component)) {
	    discrepancy <- discrepancy + abs(predicted.module.component[j])
	  }
	  discrepancy <- discrepancy / length(predicted.module.component)

	  # Добавляем полученное значение в predicated.quality
	  predicted.quality <- rbind(predicted.quality, c("module_category"=module,
	    "component_category"=component, "avg_discrepancy"=discrepancy))  
	}